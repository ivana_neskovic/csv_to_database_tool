
CREATE TABLE Employees (
    id INT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    hire_date DATE,
    job_title VARCHAR(100),
    department VARCHAR(100),
    birthdate DATE,
    gender VARCHAR(1),
    marital_status VARCHAR(20),
    education VARCHAR(50),
    jmbg VARCHAR(13),
    supervisor_id INT,
    salary_amount DECIMAL(10, 2),
    hash_value VARCHAR(64) UNIQUE
);

CREATE TABLE Contact (
    contact_id INT PRIMARY KEY,
    employee_id INT,
    email VARCHAR(100),
    phone_number VARCHAR(20),
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Projects (
    project_id INT PRIMARY KEY,
    project_name VARCHAR(100),
    start_date DATE,
    end_date DATE
);


CREATE TABLE Roles (
    role_id INT PRIMARY KEY,
    role_name VARCHAR(100)
);


CREATE TABLE Performance (
    performance_id INT PRIMARY KEY,
    employee_id INT,
    score INT,
    review_date DATE,
    comments VARCHAR(100),
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Attendance (
    attendance_id INT PRIMARY KEY,
    employee_id INT,
    attendance_date DATE,
    attendance_status VARCHAR(20),
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Training (
    training_id INT PRIMARY KEY,
    training_name VARCHAR(100),
    employee_id INT,
    training_date DATE,
    training_duration INT,
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Benefits (
    benefit_id INT PRIMARY KEY,
    employee_id INT,
    benefit_type VARCHAR(100),
    benefit_value DECIMAL(10, 2),
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Salaries (
    salary_id INT PRIMARY KEY,
    employee_id INT,
    salary_amount DECIMAL(10, 2),
    salary_date DATE,
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Projects_Employees (
    project_employee_id INT PRIMARY KEY,
    project_id INT,
    employee_id INT,
    FOREIGN KEY (project_id) REFERENCES Projects(project_id),
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);


CREATE TABLE Roles_Employees (
    role_employee_id INT PRIMARY KEY,
    role_id INT,
    employee_id INT,
    FOREIGN KEY (role_id) REFERENCES Roles(role_id),
    FOREIGN KEY (employee_id) REFERENCES Employees(id)
);
